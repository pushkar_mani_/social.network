package com.social.network.repository;

import com.social.network.model.Friends;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface FriendRepository extends JpaRepository<Friends, Long> {
}
