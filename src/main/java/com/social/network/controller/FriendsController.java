package com.social.network.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.social.network.model.Friends;
import com.social.network.repository.FriendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.social.network.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/v1")
public class FriendsController {
    @Autowired
    private FriendRepository friendsRepository;

    @GetMapping("/friends")
    public List<Friends> getAllFriends() {
        return friendsRepository.findAll();
    }

    @GetMapping("/friends/{id}")
    public ResponseEntity<Friends> getFriendsById(@PathVariable(value = "id") Long friendId)
            throws ResourceNotFoundException {
        Friends friend = friendsRepository.findById(friendId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + friendId));
        return ResponseEntity.ok().body(friend);
    }

    @PostMapping("/friends")
    public Friends createFriend(@Valid @RequestBody Friends friends) {
        return friendsRepository.save(friends);
    }

    @PutMapping("/friends/{id}")
    public ResponseEntity<Friends> updateFriend(@PathVariable(value = "id") Long friendId,
                                                   @Valid @RequestBody Friends friendsDetails) throws ResourceNotFoundException {
        Friends friends = friendsRepository.findById(friendId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + friendId));


        final Friends updatedEmployee = friendsRepository.save(friends);
        return ResponseEntity.ok(updatedEmployee);
    }

    @DeleteMapping("/friends/{id}")
    public Map<String, Boolean> deletefriends(@PathVariable(value = "id") Long friendId)
            throws ResourceNotFoundException {
        Friends friends = friendsRepository.findById(friendId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + friendId));

        friendsRepository.delete(friends);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
