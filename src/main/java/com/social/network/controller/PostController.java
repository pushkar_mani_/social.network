package com.social.network.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.social.network.model.Friends;
import com.social.network.model.Post;
import com.social.network.model.User;
import com.social.network.repository.FriendRepository;
import com.social.network.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.social.network.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/v1")
public class PostController {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private FriendRepository friendsRepository;

    @GetMapping("/post")
    public List<Post> getPost() {
        return postRepository.findAll();
    }

    @GetMapping("/post/{id}")
    public ResponseEntity<Post> getPostById(@PathVariable(value = "id") Long postId)
            throws ResourceNotFoundException {
        List<Friends> friends=friendsRepository.findAll();

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + postId));
        return ResponseEntity.ok().body(post);
    }

    @PostMapping("/post")
    public Post createpost(@Valid @RequestBody Post post) {
        return postRepository.save(post);
    }


    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deletePost(@PathVariable(value = "id") Long postId)
            throws ResourceNotFoundException {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + postId));

        postRepository.delete(post);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
