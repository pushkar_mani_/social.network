package com.social.network.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "firends")
public class Friends {

    private long id;
    private long sender_user_id;
    private long receiver_user_id;
    public Friends() {

    }

@Column(name = "sender_user_id")
    public long getSender_user_id() {
        return sender_user_id;
    }

    public void setSender_user_id(long sender_user_id) {
        this.sender_user_id = sender_user_id;
    }
@Column(name = "receiver_user_id")
    public long getReceiver_user_id() {
        return receiver_user_id;
    }

    public void setReceiver_user_id(long receiver_user_id) {
        this.receiver_user_id = receiver_user_id;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }



}
